-- auto-generated definition
create table trainings
(
    id             integer not null
        constraint trainings_pkey
            primary key,
    kilometers     real,
    duration       time,
    calories       integer,
    elevation_gain integer,
    max_elevation  integer,
    average_speed  real,
    max_speed      real,
    average_power  integer,
    training_date  date,
    gap            integer,
    extra_info     varchar(23)
);

alter table trainings
    owner to postgres;


