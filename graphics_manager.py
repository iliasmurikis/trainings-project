import kivy
from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.popup import Popup
from kivy.lang import Builder
from kivy.uix.recycleview import RecycleView
from kivy.uix.recycleview.views import RecycleDataViewBehavior
from kivy.uix.boxlayout import BoxLayout
import sys
import psycopg2
import datetime
import pandas as pd
from kivy.uix.widget import Widget

table_names = ["id", "kilometers", "duration", "calories", "elevation gain", "max elevation", "average speed",
               "max speed", "average power", "training date", "gap", "extra info"]

# print(kivy.__version__)
kivy.require("1.11.1")

# Connect to database
while True:
    try:
        conn = psycopg2.connect(
            host="localhost",
            database="Project_Trainings",
            user="postgres",
            port="5432"
        )
        cur = conn.cursor()
        print("Connection Established")
        break
    except Exception as error:
        print(str(error))  # executes connection with database

Builder.load_string('''
<ShowTrainingsPage>:
    name: 'Label'
    rv: rv
    orientation: "vertical"
    BoxLayout:
        orientation: "vertical"
        
        RecycleView:
            size_hint_y: 0.9
            id: rv
            viewclass: 'Label'
            RecycleBoxLayout:
                default_size: None, dp(23)
                default_size_hint: 1, None
                size_hint_y: None
                height: self.minimum_height
                orientation: 'vertical'
                
        Button:
            size_hint_y: 0.1
            text:    'Return'
            on_press: root.return_button_function(self)
            
<CustomQueryPage>:
    name: 'Label'
    rv: rv
    orientation: "vertical"
    BoxLayout:
        orientation: "vertical"
        
        # Text input label
        
        # Button for execution
        
        RecycleView:
            size_hint_y: 0.9
            id: rv
            viewclass: 'Label'
            RecycleBoxLayout:
                default_size: None, dp(23)
                default_size_hint: 1, None
                size_hint_y: None
                height: self.minimum_height
                orientation: 'vertical'
                
        Button:
            size_hint_y: 0.1
            text:    'Return'
            on_press: root.return_button_function(self)
            
''')


class FunctionsCollection:
    def select_all_beautyfie(self):
        try:
            cur.execute("select * from trainings order by id asc;")
            # rows = cur.fetchall()
            # data = []
            # temp = ''
            # for row in rows:
            #     for i in range(len(row) - 1):
            #         temp = temp + str(table_names[i] + " " + f"{row[i]}" + "|")
            #     temp = temp + str(table_names[-1] + " " + f"{row[-1]}")     # + "\n"
            #     data.append(str(temp))
            # print(data)
            return cur.fetchall()
        except Exception as e:
            print(str(e))

    def get_max_id(self):
        try:
            cur.execute("select max(id) from trainings;")
            return cur.fetchone()[0]
        except Exception as error:
            print(str(error))

    def get_last_training_date(self):
        try:
            cur.execute("select max(training_date) from trainings;")
            return cur.fetchone()
        except Exception as error:
            print(str(error))

    def select_all_pure(self):
        cur.execute("select * from trainings order by id desc;")
        return cur.fetchall()

    def export_to_csv(self):
        try:
            rows = FunctionsCollection.select_all_pure(self)
            df = pd.DataFrame(data=rows, columns=table_names)
            df.to_csv('/home/none/Downloads/cycling.csv')
        except Exception as error:
            print(str(error))

    def custom_query(self):
        try:
            names = '|'
            dashes = ''
            for i in table_names:
                names += str(i) + '|'
            for j in names:
                dashes += '_'
            print('\n\nTable: trainings')
            print(dashes + '\n' + names + '\n' + dashes)
            query = input('Enter your query:\n')
            cur.execute(query)
            rows = cur.fetchall()
            for i in rows:
                print(str(i))
        except Exception as error:
            print(str(error))

    def new_custom_query(self, query):
        try:
            cur.execute(str(query))
            return cur.fetchall()
        except Exception as error:
            print(str(error))

class MainPage(GridLayout):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.cols = 2

        self.show_button = Button(text="Show")
        self.show_button.bind(on_press=self.show_button_function)

        self.add_widget(Label(text="Show Trainings:"))
        self.add_widget(self.show_button)
        # -----------------------------------------------------------
        self.add_button = Button(text="Add")
        self.add_button.bind(on_press=self.add_button_function)

        self.add_widget(Label(text="Add Training:"))
        self.add_widget(self.add_button)
        # -----------------------------------------------------------
        self.export_button = Button(text="Export")
        self.export_button.bind(on_press=self.export_button_function)

        self.add_widget(Label(text="Export Trainings:"))
        self.add_widget(self.export_button)
        # -----------------------------------------------------------
        self.custom_button = Button(text="Execute")
        self.custom_button.bind(on_press=self.custom_button_function)

        self.add_widget(Label(text="Execute Custom Query:"))
        self.add_widget(self.custom_button)
        # -----------------------------------------------------------
        self.exit_button = Button(text="Exit")
        self.exit_button.bind(on_press=self.exit_button_function)

        self.add_widget(Label(text="Exit App:"))
        self.add_widget(self.exit_button)

    @staticmethod
    def show_button_function(self):
        trainings_app.screen_manager.transition.direction = "left"
        trainings_app.screen_manager.current = "ShowTrainings"

    @staticmethod
    def add_button_function(self):
        trainings_app.screen_manager.transition.direction = "left"
        trainings_app.screen_manager.current = "AddTraining"

    @staticmethod
    def custom_button_function(self):
        trainings_app.screen_manager.transition.direction = "left"
        trainings_app.screen_manager.current = "CustomQuery"

    @staticmethod
    def export_button_function(self):
        FunctionsCollection.export_to_csv(self)

        layout = GridLayout(cols=1, padding=10)

        closeButton = Button(text="Close")

        layout.add_widget(Label(text="Exportation Completed"))
        layout.add_widget(closeButton)

        popup = Popup(title='Notification',
                      content=layout,
                      size_hint=(None, None), size=(200, 200))
        popup.open()

        # Attach close button press with popup.dismiss action
        closeButton.bind(on_press=popup.dismiss)

    @staticmethod
    def exit_button_function(self):
        conn.commit()
        print("Connection commited")
        cur.close()
        print("Cursor closed")
        conn.close()
        print("Connection Closed")
        print("Exiting...")
        sys.exit()


class ShowTrainingsPage(RecycleView):
    def __init__(self, **kwargs):
        super(ShowTrainingsPage, self).__init__(**kwargs)

        # Get records
        data_in_table = FunctionsCollection.select_all_pure(self)
        show_time = []
        temp = ''
        for row in data_in_table:
            for i in range(len(row) - 1):
                temp = temp + str(f"{row[i]}" + " | ")
            temp = temp + str(f"{row[-1]}")     # + "\n"
            show_time.append(temp)
            temp = ''

        self.rv.data = [{'text': str(x)} for x in show_time]

    @staticmethod
    def return_button_function(self):
        trainings_app.screen_manager.current = "Main"
        trainings_app.screen_manager.transition.direction = "right"


class AddTrainingPage(GridLayout):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.cols = 2

        # Kilometers
        self.add_widget(Label(text="Enter kilometers:"))
        self.kms = TextInput(multiline=False)
        self.add_widget(self.kms)
        # Duration
        self.add_widget(Label(text="Enter duration(HH:MM:SS):"))
        self.dur = TextInput(multiline=False)
        self.add_widget(self.dur)
        # Calories
        self.add_widget(Label(text="Enter calories:"))
        self.cal = TextInput(multiline=False)
        self.add_widget(self.cal)
        # Elevation Gain
        self.add_widget(Label(text="Enter elevation gain:"))
        self.elev = TextInput(multiline=False)
        self.add_widget(self.elev)
        # Maximum Elevation
        self.add_widget(Label(text="Enter maximum elevation gain:"))
        self.max_elev = TextInput(multiline=False)
        self.add_widget(self.max_elev)
        # Average Speed
        self.add_widget(Label(text="Enter average speed:"))
        self.avg_sp = TextInput(multiline=False)
        self.add_widget(self.avg_sp)
        # Maximum Speed
        self.add_widget(Label(text="Enter maximum speed:"))
        self.max_sp = TextInput(multiline=False)
        self.add_widget(self.max_sp)
        # Average Power
        self.add_widget(Label(text="Enter average power:"))
        self.avg_pwr = TextInput(multiline=False)
        self.add_widget(self.avg_pwr)
        # Training Date
        self.add_widget(Label(text="Enter date(YYYY-MM-DD):"))
        self.date = TextInput(multiline=False)
        self.add_widget(self.date)
        # Notes
        self.add_widget(Label(text="Enter extra notes(notes should be inside '...')(optional):"))
        self.notes = TextInput(multiline=False)
        self.add_widget(self.notes)

        self.add_widget(Label())
        self.add_widget(Label())

        # Add Training
        self.add_training_button = Button(text="Add Training")
        self.add_training_button.bind(on_press=self.add_training_button_function)

        self.add_widget(Label())
        self.add_widget(self.add_training_button)

        # -------------------------------------
        # Return
        self.return_button = Button(text="Return")
        self.return_button.bind(on_press=self.return_button_function)

        self.add_widget(Label())
        self.add_widget(self.return_button)

    def add_training_button_function(self, instance):
        try:
            # Get last training's id and add one for the current training
            id = FunctionsCollection.get_max_id(self) + 1
            kms = self.kms.text
            my_dur = self.dur.text
            # String dur into datetime dur
            dur = datetime.datetime.strptime(my_dur, "%H:%M:%S").time()
            cal = self.cal.text
            elev = self.elev.text
            max_elev = self.max_elev.text
            avg_sp = self.avg_sp.text
            max_sp = self.max_sp.text
            avg_pwr = self.avg_pwr.text
            date = self.date.text
            year, month, day = map(int, date.split("-"))
            current_date = datetime.date(year, month, day)
            # Get last training date
            last_date = FunctionsCollection.get_last_training_date(self)
            temp = last_date[0]
            # Calculate the gap between two trainings
            gap = current_date - temp
            gap_string = str(gap)
            gap_int = int(gap_string[0])

            if self.notes.text:
                notes = ","+self.notes.text
                query = "insert into trainings(id,kilometers,duration,calories,elevation_gain," \
                        "max_elevation,average_speed,max_speed,average_power,training_date,gap,extra_info) " \
                        "values(" + str(id) + "," + str(kms) + ",'" + str(dur) + "'," + str(cal) + "," + str(elev) \
                        + "," + str(max_elev) + "," + str(avg_sp) + "," + str(max_sp) + "," + str(avg_pwr) + ",'" \
                        + str(date) + "'," + str(gap_int) + notes + ")"
            else:
                notes = self.notes.text
                query = "insert into trainings(id,kilometers,duration,calories,elevation_gain," \
                        "max_elevation,average_speed,max_speed,average_power,training_date,gap) " \
                        "values(" + str(id) + "," + str(kms) + ",'" + str(dur) + "'," + str(cal) + "," + str(elev) \
                        + "," + str(max_elev) + "," + str(avg_sp) + "," + str(max_sp) + "," + str(avg_pwr) + ",'" \
                        + str(date) + "'," + str(gap_int) + notes + ")"

            try:
                cur.execute(query)
                # Reset field values, only when addition completed successfully
                self.kms.text = ''
                self.dur.text = ''
                self.dur.text = ''
                self.cal.text = ''
                self.elev.text = ''
                self.max_elev.text = ''
                self.avg_sp.text = ''
                self.max_sp.text = ''
                self.avg_pwr.text = ''
                self.date.text = ''
                self.notes.text = ''
            except Exception as e:
                print(str(e))
        except Exception as e:
            print(str(e))

    @staticmethod
    def return_button_function(self):
        trainings_app.screen_manager.transition.direction = "right"
        trainings_app.screen_manager.current = "Main"


# class CustomQueryPage(GridLayout):
#     def __init__(self):
#         super().__init__()
#         self.cols = 2
#
#         self.add_widget(Label())
#         self.add_widget(Label())
#
#         self.return_button = Button(text="Return")
#         self.return_button.bind(on_press=self.return_button_function)
#
#         self.add_widget(Label())
#         self.add_widget(self.return_button)
#
#     @staticmethod
#     def return_button_function(self):
#         trainings_app.screen_manager.transition.direction = "right"
#         trainings_app.screen_manager.current = "Main"

class CustomQueryPage(RecycleView):
    def __init__(self, **kwargs):
        super(CustomQueryPage, self).__init__(**kwargs)

        # Get user's input
        query = 'select * from trainings'

        # Execute query
        data_in_table = FunctionsCollection.new_custom_query(self, query)

        # Manipulate output
        show_time = []
        temp = ''
        for row in data_in_table:
            for i in range(len(row) - 1):
                temp = temp + str(f"{row[i]}" + " | ")
            temp = temp + str(f"{row[-1]}")     # + "\n"
            show_time.append(temp)
            temp = ''

        self.rv.data = [{'text': str(x)} for x in show_time]

    @staticmethod
    def return_button_function(self):
        trainings_app.screen_manager.current = "Main"
        trainings_app.screen_manager.transition.direction = "right"



class TrainingsApp(App):
    def build(self):
        self.screen_manager = ScreenManager()


        self.main_page = MainPage()
        screen = Screen(name="Main")
        screen.add_widget(self.main_page)
        trainings_app.screen_manager.transition.direction = "left"
        self.screen_manager.add_widget(screen)

        self.show_page = ShowTrainingsPage()
        screen = Screen(name="ShowTrainings")
        screen.add_widget(self.show_page)
        trainings_app.screen_manager.transition.direction = "left"
        self.screen_manager.add_widget(screen)

        self.add_page = AddTrainingPage()
        screen = Screen(name="AddTraining")
        screen.add_widget(self.add_page)
        self.screen_manager.add_widget(screen)

        self.custom_page = CustomQueryPage()
        screen = Screen(name="CustomQuery")
        screen.add_widget(self.custom_page)
        self.screen_manager.add_widget(screen)

        return self.screen_manager


if __name__ == '__main__':
    trainings_app = TrainingsApp()
    trainings_app.run()
