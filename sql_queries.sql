select extract(month from training_date) as my_month, sum(kilometers) as my_sum
from trainings
group by my_month
order by my_sum asc;

select n.my_year, cast(n.kms_sum as decimal) / cast(n.trains_sum as decimal) as average
from (
	select extract(year from training_date) as my_year, sum(kilometers) as kms_sum, count(id) as trains_sum
	from trainings
	group by my_year
	order by my_year asc) as n;
